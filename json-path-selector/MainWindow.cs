﻿using System;
using System.IO;
using Newtonsoft;
using Gtk;
using Newtonsoft.Json.Linq;
using System.Text;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    string _jsonContent = "";
    string _jsonLocation = "";
    string _jsonPath = "";

    private void LoadJsonContent()
    {

    }

    private void ResetJsonContent()
    {
        _jsonContent = "";
    }

    protected void OnFileLocationChanged(object sender, EventArgs e)
    {
        _jsonLocation = fileLocation.Text;
    }

    protected void OnFindFileClicked(object sender, EventArgs e)
    {
        ResetJsonContent();

        if (!File.Exists(_jsonLocation)) {
            return;
        }

        _jsonContent = File.ReadAllText(_jsonLocation);
    }

    protected void OnJsonPathChanged(object sender, EventArgs e)
    {
        _jsonPath = jsonPath.Text.Trim();

        if(string.IsNullOrWhiteSpace(_jsonPath) || string.IsNullOrWhiteSpace(_jsonContent))
        {
            return;
        }

        try
        {
            var jobject = JObject.Parse(_jsonContent);
            var found = jobject.SelectTokens(_jsonPath);
            var b = new StringBuilder();

            b.Append('[').AppendLine();
            var first = true;
            foreach(var token in found)
            {
                b.Append(token.ToString());
                if (!first)
                {
                    b.Append(',');
                }
                first = false;
                b.AppendLine();
            }
            b.Append(']').AppendLine();

            jsonResult.Buffer.Text = b.ToString();
            errorPanel.Text = "";
        }
        catch(Exception err)
        {
            errorPanel.Text = err.Message;
        }

    }
}
